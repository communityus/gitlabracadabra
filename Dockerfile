FROM debian:buster

COPY . /tmp/app

WORKDIR /tmp/app

RUN set -x && \
    apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y --no-install-recommends \
      python3 \
      python3-pip \
      python3-setuptools \
    && rm -rf /var/lib/apt/lists/* && \
    pip3 install -r requirements.txt && \
    python3 setup.py build && \
    python3 setup.py install && \
    adduser --disabled-password --gecos '' gitlabracadabra

USER gitlabracadabra

ENTRYPOINT ["gitlabracadabra"]
