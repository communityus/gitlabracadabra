# Table of Contents

- [Project lifecycle](#project-lifecycle)
- [General Settings](#general-settings)
  - [Naming, topics, avatar](#naming-topics-avatar)
  - [Visibility, project features, permissions](#visibility-project-features-permissions)
  - [Merge requests](#merge-requests)
  - [Merge request approvals](#merge-request-approvals)
- [Members](#members)
- [Repository Settings](#repository-settings)
  - [Default branch](#default-branch)
  - [Mirroring repositories](#mirroring-repositories)
  - [Protected Branches](#protected-branches)
  - [Protected Tags](#protected-tags)
  - [Branches](#branches)
- [CI / CD Settings](#ci-cd-settings)
  - [General pipelines](#general-pipelines)
  - [Auto DevOps](#auto-devops)
  - [Runners](#runners)
  - [Variables](#variables)
- [Labels](#labels)
- [Milestones](#milestones)
- [Deprecated](#deprecated)
- [Undocumented](#undocumented)

# Project lifecycle

`create_object` Create object if it does not exists:
```yaml
mygroup/myproject:
  create_object: true # or false
```

`delete_object` Delete object if it exists:
```yaml
mygroup/myproject:
  delete_object: true # or false
```

`archived` Archive or unarchive project:
```yaml
mygroup/myproject:
  archived: true # or false
```

`initialize_with_readme` false by default:
```yaml
mygroup/myproject:
  initialize_with_readme: true # or false
```

# General Settings

## Naming, topics, avatar

`name` Project name:
```yaml
mygroup/myproject:
  name: My name
```

`tag_list` Topics:
```yaml
mygroup/myproject:
  tag_list: [GitLab, API, YAML]
```

`description` Project description:
```yaml
mygroup/myproject:
  description: |-
    🧹 GitLabracadabra 🧙

    :alembic: Adds some magic to GitLab :crystal\_ball:
```

## Visibility, project features, permissions

`visibility` Project visibility:
```yaml
mygroup/myproject:
  visibility: private # one of private, internal, public
```

`request_access_enabled` Allow users to request access:
```yaml
mygroup/myproject:
  request_access_enabled: true # or false
```

`issues_access_level` Issues access level.:
```yaml
mygroup/myproject:
  issues_access_level: disabled # one of disabled, private, enabled
```

`repository_access_level` Repository access level.:
```yaml
mygroup/myproject:
  repository_access_level: disabled # one of disabled, private, enabled
```

`merge_requests_access_level` Merge requests access level.:
```yaml
mygroup/myproject:
  merge_requests_access_level: disabled # one of disabled, private, enabled
```

`builds_access_level` Builds access level.:
```yaml
mygroup/myproject:
  builds_access_level: disabled # one of disabled, private, enabled
```

`container_registry_enabled` Enable container registry for this project:
```yaml
mygroup/myproject:
  container_registry_enabled: true # or false
```

`lfs_enabled` Enable LFS:
```yaml
mygroup/myproject:
  lfs_enabled: true # or false
```

`packages_enabled` Enable or disable packages repository feature:
```yaml
mygroup/myproject:
  packages_enabled: true # or false
```

`wiki_access_level` Wiki access level.:
```yaml
mygroup/myproject:
  wiki_access_level: disabled # one of disabled, private, enabled
```

`snippets_access_level` Snippets access level.:
```yaml
mygroup/myproject:
  snippets_access_level: disabled # one of disabled, private, enabled
```

## Merge requests

`merge_method` Set the merge method used:
```yaml
mygroup/myproject:
  merge_method: merge # one of merge, rebase_merge, ff
```

`resolve_outdated_diff_discussions` Automatically resolve merge request diffs discussions on lines changed with a push:
```yaml
mygroup/myproject:
  resolve_outdated_diff_discussions: true # or false
```

`printing_merge_request_link_enabled` Show link to create/view merge request when pushing from the command line:
```yaml
mygroup/myproject:
  printing_merge_request_link_enabled: true # or false
```

`remove_source_branch_after_merge` Enable Delete source branch option by default for all new merge requests:
```yaml
mygroup/myproject:
  remove_source_branch_after_merge: true # or false
```

`only_allow_merge_if_pipeline_succeeds` Set whether merge requests can only be merged with successful jobs:
```yaml
mygroup/myproject:
  only_allow_merge_if_pipeline_succeeds: true # or false
```

`only_allow_merge_if_all_discussions_are_resolved` Set whether merge requests can only be merged when all the discussions are resolved:
```yaml
mygroup/myproject:
  only_allow_merge_if_all_discussions_are_resolved: true # or false
```

## Merge request approvals

`approvals_before_merge` How many approvers should approve merge request by default:
```yaml
mygroup/myproject:
  approvals_before_merge: 42
```

# Members

`members` Members:
```yaml
mygroup/myproject:
  members: 
    foo: developer
    bar: maintainer # one of guest, reporter, developer, maintainer, owner

```

`unknown_members` What to do with unknown members (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_members: warn # one of warn, delete, remove, ignore, skip
```

`groups` Groups:
```yaml
mygroup/myproject:
  groups: 
    group/foo: guest
    group/bar: reporter # one of guest, reporter, developer, maintainer

```

`unknown_groups` What to do with unknown groups (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_groups: warn # one of warn, delete, remove, ignore, skip
```

# Repository Settings

## Default branch

`default_branch` `master` by default:
```yaml
mygroup/myproject:
  default_branch: My default branch
```

## Mirroring repositories

`mirrors` The list of project's mirrors:
```yaml
mygroup/myproject:
  mirrors: 
    - url: https://gitlab.com/gitlabracadabra/gitlabracadabra.git
      direction: pull # one of pull, push ; only first pull mirror is processed
  builds_access_level: disabled # Recommended to prevent triggering pipelines on push
```

`mirror` Enables pull mirroring in a project:
```yaml
mygroup/myproject:
  mirror: true # or false
```

`import_url` URL to import repository from:
```yaml
mygroup/myproject:
  import_url: My import url
```

`mirror_user_id` User responsible for all the activity surrounding a pull mirror event:
```yaml
mygroup/myproject:
  mirror_user_id: 42
```

`mirror_overwrites_diverged_branches` Pull mirror overwrites diverged branches:
```yaml
mygroup/myproject:
  mirror_overwrites_diverged_branches: true # or false
```

`mirror_trigger_builds` Pull mirroring triggers builds:
```yaml
mygroup/myproject:
  mirror_trigger_builds: true # or false
```

`only_mirror_protected_branches` Only mirror protected branches:
```yaml
mygroup/myproject:
  only_mirror_protected_branches: true # or false
```

## Protected Branches

`protected_branches` Protected branches:
```yaml
mygroup/myproject:
  protected_branches: 
    master:
      merge_access_level: maintainer # one of noone, developer, maintainer
      push_access_level: noone
    develop:
      merge_access_level: developer
      push_access_level: noone

```

`unknown_protected_branches` What to do with unknown protected branches (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_protected_branches: warn # one of warn, delete, remove, ignore, skip
```

## Protected Tags

`protected_tags` Protected tags:
```yaml
mygroup/myproject:
  protected_tags: 
    v*: maintainer # one of noone, developer, maintainer
    *: developer # one of noone, developer, maintainer

```

`unknown_protected_tags` What to do with unknown protected tags (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_protected_tags: warn # one of warn, delete, remove, ignore, skip
```

## Branches

`branches` The list of branches for a project. Branches are created in order.:
```yaml
mygroup/myproject:
  branches: 
    - master
    - develop
```

# CI / CD Settings

## General pipelines

`build_git_strategy` The Git strategy:
```yaml
mygroup/myproject:
  build_git_strategy: fetch # one of fetch, clone
```

`ci_default_git_depth` Default number of revisions for shallow cloning:
```yaml
mygroup/myproject:
  ci_default_git_depth: 42
```

`build_timeout` The maximum amount of time in minutes that a job is able run (in seconds):
```yaml
mygroup/myproject:
  build_timeout: 42
```

`ci_config_path` The path to CI config file:
```yaml
mygroup/myproject:
  ci_config_path: debian/salsa-ci.yml
```

`public_builds` If true, jobs can be viewed by non-project-members:
```yaml
mygroup/myproject:
  public_builds: true # or false
```

`auto_cancel_pending_pipelines` Auto-cancel pending pipelines:
```yaml
mygroup/myproject:
  auto_cancel_pending_pipelines: enabled # one of enabled, disabled
```

`build_coverage_regex` Test coverage parsing:
```yaml
mygroup/myproject:
  build_coverage_regex: My build coverage regex
```

## Auto DevOps

`auto_devops_enabled` Enable Auto DevOps for this project:
```yaml
mygroup/myproject:
  auto_devops_enabled: true # or false
```

`auto_devops_deploy_strategy` Auto Deploy strategy:
```yaml
mygroup/myproject:
  auto_devops_deploy_strategy: continuous # one of continuous, manual, timed_incremental
```

## Runners

`shared_runners_enabled` Enable shared runners for this project:
```yaml
mygroup/myproject:
  shared_runners_enabled: true # or false
```

## Variables

`variables` The list of project's variables:
```yaml
mygroup/myproject:
  variables: 
    - key: DAST_DISABLED
      value: '1'
      masked: false
      protected: false
      variable_type: env_var

```

`unknown_variables` What to do with unknown variables (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_variables: warn # one of warn, delete, remove, ignore, skip
```

# Labels

`labels` The list of project's labels:
```yaml
mygroup/myproject:
  labels: 
    - name: critical
      priority: 0
    - name: bug
      priority: 1
    - name: confirmed
      priority: 2

```

`unknown_labels` What to do with unknown labels (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_labels: warn # one of warn, delete, remove, ignore, skip
```

# Milestones

`milestones` The list of project's milestones:
```yaml
mygroup/myproject:
  milestones: 
    - title: '1.0'
      description: Version 1.0
      due_date: '2021-01-23' # Quotes are mandatory
      start_date: '2020-01-23' # Quotes are mandatory
      state: active # or closed

```

`unknown_milestones` What to do with unknown milestones (`warn` by default).:
```yaml
mygroup/myproject:
  unknown_milestones: warn # one of warn, delete, remove, ignore, skip
```

# Deprecated

`issues_enabled` Enable issues for this project:
```yaml
mygroup/myproject:
  issues_enabled: true # or false
```

`merge_requests_enabled` Enable merge requests for this project:
```yaml
mygroup/myproject:
  merge_requests_enabled: true # or false
```

`jobs_enabled` Enable jobs for this project:
```yaml
mygroup/myproject:
  jobs_enabled: true # or false
```

`wiki_enabled` Enable wiki for this project:
```yaml
mygroup/myproject:
  wiki_enabled: true # or false
```

`snippets_enabled` Enable snippets for this project:
```yaml
mygroup/myproject:
  snippets_enabled: true # or false
```

# Undocumented

`avatar` Project avatar:
```yaml
mygroup/myproject:
  avatar: My avatar
```

`external_authorization_classification_label` The classification label for the project:
```yaml
mygroup/myproject:
  external_authorization_classification_label: My external authorization classification label
```

`repository_storage` Which storage shard the repository is on. Available only to admins:
```yaml
mygroup/myproject:
  repository_storage: My repository storage
```


