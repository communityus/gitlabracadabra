# Change Log

## 0.5.0 (2020-01-23)

**Backwards incompatible changes:**

- `extends` now use `deep` merging by default, to restore the previous behavior
  use `replace` merge strategy

**New features:**

- `extends` now supports two merge strategies: `deep` (the default) and `replace` (!54 and !56)
- `extends` now supports multiple parents (!56)
- Repository mirroring (!52, !60)
- Add support for project and group milestones (!58)

**Enhancements:**

- Add remove_source_branch_after_merge parameter to projects (!46)
- Add default_ci_config_path parameter to application settings (!47)
- Add emails_disabled, project_creation_level, subgroup_creation_level, require_two_factor_authentication,
  two_factor_grace_period, auto_devops_enabled to groups (!59)
- Sync with current project and group parameters (!59)
- Documentation:
  - Add Table of Contents (!48)
  - Generate markdown doc from source (!48)
  - Document releasing a new version (!49)
  - Various documentation enhancements (!51)
  - Add general action file documentation (!53)
- Tests:
  - Only install packages for the gitlabracadabra.tests.unit package (!55)

**Fixes:**

- User:
  - Fix typo of user param: s/admin/is_admin/ (!57)
  - Remove username as param, as this is the id (!57)
  - Fix CREATE_KEY handling (!57)
  - user's skip_confirmation is a create param (!57)
  - Do not try to change create-only params for users (!57)
- Tests:
  - Clean up before testing group member not found (!50)

## 0.4.0 (2019-11-14)

**New features:**

- Add delete\_object param (!43)
- Add support for include (#7, !42)
- Add project and group labels support (#2, !36, !39, !40)
- Add support for application settings (#4, !34)
- Package uploaded to Debian (!45)

**Enhancements:**

- Add support for latest project params (!33)
- Test YAML file loading (!42)
- Ensure that content is not changed during processing (!41)
- User password is only set on user creation (!41)
- Assert all cassettes are "all played" (!37, !38)

**Fixes:**

- Don't fail when some variables params are not available (!44)
- Python-gitlab 1.13 still has incorrect labels support (!45)

## 0.2.1 (2019-06-20)

**New features:**

- Debian packages (!24, !25)
- Project and group variables (!29)
- Project archived parameter (!27)

**Enhancements:**

- Catch members deletion errors (!30)
- Skip changing parameter only when not available (i.e. handle null as current value) (!28)
- Catch REST errors of \_process\_ methods (!26)
- Enforce draft 4 of jsonschema (!25)
- Skip protected tags tests on python-gitlab < 1.7.0 (!24)
- Workaround "all=True" in API calls with python-gitlab < 1.8.0 (!24)
- Move VCR fixtures closer to the tests (!23)
- Centralize VCR configuration (!23)

## 0.1.0 (2019-05-25)

**New features:**

- Projects
  - Basic parameters
  - Project members
  - Add protected branches support
  - Protected tags
  - Add latest project parameters
    (mirror\_user\_id, only\_mirror\_protected\_branches, mirror\_overwrites\_diverged\_branches, packages\_enabled)
  - Add initialize\_with\_readme support to projects
- Groups:
  - Basic parameters
  - Group members
  - Add latest group parameters
    (membership\_lock, share\_with\_group\_lock, file\_template\_project\_id, extra\_shared\_runners\_minutes\_limit)
- Users:
  - Basic parameters
